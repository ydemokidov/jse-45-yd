package com.t1.yd.tm.api.repository.model;

import com.t1.yd.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(@NotNull E entity);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    Collection<E> set(@NotNull Collection<E> collection);

    @NotNull
    Collection<E> add(@NotNull Collection<E> collection);

    @NotNull
    List<E> findAll(@NotNull Comparator comparator);

    @Nullable
    E findOneById(@NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull Integer index);

    E removeById(@NotNull String id);

    E removeByIndex(@NotNull Integer index);

    boolean existsById(@NotNull String id);

    int getSize();

    E update(@NotNull E entity);

    E remove(@NotNull E entity);

}
