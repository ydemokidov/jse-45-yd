package com.t1.yd.tm.api.repository.model;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<User> {

    @Nullable User findByLogin(@NotNull String login);

    @Nullable User findByEmail(@NotNull String email);

}
