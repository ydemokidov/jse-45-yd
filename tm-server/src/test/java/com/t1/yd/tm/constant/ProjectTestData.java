package com.t1.yd.tm.constant;

import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class ProjectTestData {

    @NotNull
    public final static ProjectDTO USER_1_PROJECT_DTO_1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_1_PROJECT_DTO_2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT_DTO_1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT_DTO_2 = new ProjectDTO();

    @NotNull
    public final static String PROJECT_NAME = "project1 name";

    @NotNull
    public final static String PROJECT_DESCRIPTION = "project1 desc";

    @NotNull
    public final static List<ProjectDTO> ALL_PROJECT_DTOS = Arrays.asList(USER_1_PROJECT_DTO_1, USER_1_PROJECT_DTO_2, ADMIN_PROJECT_DTO_1, ADMIN_PROJECT_DTO_2);

    @NotNull
    public final static List<ProjectDTO> USER_1_PROJECT_DTOS = Arrays.asList(USER_1_PROJECT_DTO_1, USER_1_PROJECT_DTO_2);

    @NotNull
    public final static List<ProjectDTO> ADMIN_PROJECT_DTOS = Arrays.asList(ADMIN_PROJECT_DTO_1, ADMIN_PROJECT_DTO_2);

}
